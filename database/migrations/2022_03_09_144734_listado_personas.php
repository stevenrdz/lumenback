<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $timestamps = false;

    public function up()
    {
        Schema::create('listadoPersonas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->string('apellido',30);
            $table->string('usuario',10)->unique();
            $table->string('correo',50)->unique();
            $table->foreignId('idRol') // UNSIGNED BIG INT
                ->references('id')
                ->on('rol');
            $table->foreignId('idEstado') // UNSIGNED BIG INT
                ->references('id')
                ->on('estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listadoPersonas');
    }
};
