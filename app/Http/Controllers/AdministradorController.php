<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Administrador;
use App\Models\ListadoPersonas;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\VarDumper;

class AdministradorController extends Controller{

  public $timestamps = false;

  public function listar(){
    $data = Administrador::all();

    return response()->json( 
        [ 
            'estado' => true, 
            'msj' => "Consulta exitosa.", 
            'datos' => $data 
        ]);
  }

  public function registro(Request $request){
    try {

      $admin = new Administrador();
      $admin->correo = $request->correo;
      $admin->contraseña = $request->contraseña;
      $admin->created_at = date("Y-m-d H:i:s");
      $admin->updated_at =date("Y-m-d H:i:s");
      $admin->save();

      $personal = new ListadoPersonas();
      $personal->nombre=$request->nombre;
      $personal->apellido=$request->apellido;
      $personal->usuario=$request->usuario;
      $personal->correo=$request->correo;
      $personal->idRol = 1;
      $personal->idEstado= 1;
      $personal->save();

      $data = ListadoPersonas::all();
      var_dump($data);
  
      return response()->json( 
        [ 
          'estado' => true, 
          'msj' => "Administrador registrado con exito",
          'data' => $data
        ]);
    } catch (\Exception $e) {
      return response()->json( 
        [ 
          'estado' => false, 
          'msj' => $e->getMessage(),
        ]);
    }
  }

  public function login(Request $request){
      
    if(isset($request->correo) && isset($request->contraseña)){      
      if($request->correo != "" && $request->contraseña != ""){
        $flag = false;
        $query = Administrador::all();
          foreach($query as $queries){
            if($request->correo == $queries->correo 
              && $request->contraseña == $queries->contraseña){
              $flag = true;
              //var_dump($queries->correo, $queries->contraseña, $flag);
            }
          }
          if($flag){
            return response()->json(
              ['estado' => true, 'msj' => "Inicio de sesion exitoso",'datos' => $request->correo]);
          }else{
            return response()->json(
              ['estado' => false, 'msj' => "Datos incorrectos"]);
          }
      }else{
        return response()->json(
          ['estado' => false,'msj' => "Ingrese un correo electronico valido"]);
      }
      
    } 
    if(!isset($request->correo) && !isset($request->correo)){
      return response()->json(['estado' => false, 'msj' => "No existe correo electronico",]);
    } 
    
  }

}