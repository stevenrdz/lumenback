<?php
namespace App\Http\Controllers;

use App\Models\Administrador;
use Illuminate\Http\Request;
use App\Models\ListadoPersonas;
use PhpParser\Node\Stmt\TryCatch;

class ListadoPersonasController extends Controller{

  public $timestamps = false;

  public function crearUsuario(Request $request){
    try {

      $personal = new ListadoPersonas();
      $personal->nombre=$request->nombre;
      $personal->apellido=$request->apellido;
      $personal->usuario=$request->usuario;
      $personal->correo=$request->correo;
      $personal->idRol = $request->rol;
      $personal->idEstado= $request->estado;
      $personal->save();

      $data = ListadoPersonas::all();
      var_dump($data);
  
      return response()->json( 
        [ 
          'estado' => true, 
          'msj' => "Usuario creado correctamente",
          'data' => $data
        ]);
    } catch (\Exception $e) {
      return response()->json( 
        [ 
          'estado' => false, 
          'msj' => "Error al crear usuario",
          'data' => $e->getMessage(),
        ]);
    }
  
  }

  public function eliminarUsuario(Request $request){
    try{
      $personal = ListadoPersonas::find($request->id);
      
      if(isset($personal)){
        $personal->delete(); 
        $query = Administrador::all();
        $data = '';
        foreach ($query as $queries) {
          if($queries->correo == $personal->correo){
            $data = $queries->id;
          }
        }
        $admin = Administrador::find($data);
        $admin->delete();
      }else{
        return response()->json(
          [ 
            'estado' => true, 
            'msj' => "El usuario ya fue eliminado"
          ]);
      }

      return response()->json(
        [ 
          'estado' => true, 
          'msj' => "Usuario eliminado"
        ]);
    }catch (\Exception $e) {
      return response()->json(
        [ 
          'estado' => true, 
          'msj' => "Cliente eliminado",
          'data' => $e->getMessage()
        ]);
    }
  }

}