<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//ADMINISTRADOR

// ----- LOGIN Y REGISTER -------------
$router->post('/administrador/login', 'AdministradorController@login');

$router->post('/administrador/registro', 'AdministradorController@registro');

// ------ FIN LOGIN Y REGISTER --------

$router->get('/administrador/listar', 'AdministradorController@listar');


$router->post('/personas/crear', 'ListadoPersonasController@crearUsuario');

$router->delete('/personas/eliminar', 'ListadoPersonasController@eliminarUsuario');


//RESIDENTES





